document.addEventListener("DOMContentLoaded", () => {
    const cards = document.querySelectorAll(".card")
    const modal = document.querySelector(".modal")
    let modalProduct = null

    let modalData = {
        product: null,
        isOpen: false
    }

    if (modal) {
        modalProduct = modal.querySelector(".modal-product")
        const modalBgSelector = ".modal .modal-background"
        const modalButtonSelector = ".modal button"

        function modalOpen(newModalData) {
            console.log(modal)
            modalData = {
                ...modalData,
                ...newModalData
            }

            if (modalProduct) {
                modalProduct.textContent = modalData.product
            }

            modal.style.display = "flex"
        }

        function modalClose(newModalData) {
            modalData = {
                ...modalData,
                ...newModalData
            }
            modal.style.display = "none"
        }

        document.addEventListener("click", (event) => {
            if (event.target.closest(modalBgSelector) || event.target.closest(modalButtonSelector)) {
                modalClose({product: null})
            }
        })

        if (cards) {
            cards.forEach(card => {
                const buyBtn = card.querySelector("button")
                const productTitle = card.querySelector("span").textContent

                if (buyBtn) {
                    buyBtn.addEventListener("click", (event) => {
                        event.preventDefault()
                        modalOpen({product: productTitle})
                    })
                }
            })
        }
    }
})